<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Assets
|--------------------------------------------------------------------------
|
*/
$config['assets_dir']     = 'assets';
$config['back_dir'] 	  = $config['assets_dir'] . '/assets_back';
$config['front_dir']   	  = $config['assets_dir'] . '/assets_front';
$config['page_title']     = 'Kejaksaan Negeri Lampung Selatan';
/*
|--------------------------------------------------------------------------
| Upload
|--------------------------------------------------------------------------
|
*/
$config['upload_dir']     = 'upload';
$config['avatar_dir']     = $config['upload_dir'] . '/avatar';