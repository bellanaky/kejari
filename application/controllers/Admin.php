<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	function __construct(){
	parent::__construct();
		
		$this->load->model('M_Function','a');

	}	

	
	public function data_mapel()
	{

		$id_mapel = $this->input->post('id_mapel');
		$tabel = 'tb_mapel';

        if(isset($_POST['save']))
        {

	        $data = array(
	 		'id_mapel' => $id_mapel,
	 		'nama_mapel' => $this->input->post('nm_mapel'),
			'tahun_ajaran' => $this->input->post('th_ajaran')
            );

            $this->a->insert_data($tabel,$data); //akses model untuk menyimpan ke database

            $this->session->set_flashdata('sukses_input','Ditambahkan');
        	redirect($_SERVER['HTTP_REFERER']);

        } 
        
        else if (isset($_POST['ubah'])) 
        
        {

            $data = array(
            'nama_mapel' => $this->input->post('nm_mapel'),
            'tahun_ajaran' => $this->input->post('th_ajaran')
            );

            $this->a->update_data($tabel,'id_mapel',$id_mapel,$data); //akses model untuk menyimpan ke database


            $this->session->set_flashdata('sukses_ubah','Diubah');
            redirect($_SERVER['HTTP_REFERER']);

        }

        else if (isset($_POST['hapus'])) 
        
        {
             $this->a->hapus_data($tabel,'id_mapel',$id_mapel);
             $this->session->set_flashdata('sukses_hapus','Dihapus');
             redirect($_SERVER['HTTP_REFERER']);
        }

        else {
            $tbl = 'tb_mapel';
            $id_tbl = 'id_mapel';
            $kodemax = $this->a->no_otomatis($tbl,$id_tbl);
            $this->data['kodejadi']  = "MAP_".$kodemax;
            $this->data['data_mapel'] = $this->a->ambil_data($tbl,$id_tbl);
            $this->template->admin_render('back/admin/v_data_mapel',$this->data);	
        }
	}


    public function data_kelas()
    {

        $id_kelas = $this->input->post('id_kelas');
        $tabel = 'tb_kelas';

      

        if(isset($_POST['save']))
        {

            $data = array(
            'id_kelas' => $id_kelas,
            'nama_kelas' => $this->input->post('nm_kelas'),
            'id_guru' => $this->input->post('id_guru')
            );

            $this->a->insert_data($tabel,$data); //akses model untuk menyimpan ke database

            $this->session->set_flashdata('sukses_input','Ditambahkan');
            redirect($_SERVER['HTTP_REFERER']);

        } 
        
        else if (isset($_POST['ubah'])) 
        
        {

            $data = array(
            'id_kelas' => $id_kelas,
            'nama_kelas' => $this->input->post('nm_kelas'),
            'id_guru' => $this->input->post('id_guru')
            );

            $this->a->update_data($tabel,'id_kelas',$id_kelas,$data); //akses model untuk menyimpan ke database


            $this->session->set_flashdata('sukses_ubah','Diubah');
            redirect($_SERVER['HTTP_REFERER']);

        }

        else if (isset($_POST['hapus'])) 
        
        {
             $this->a->hapus_data($tabel,'id_kelas',$id_kelas);
             $this->session->set_flashdata('sukses_hapus','Dihapus');
             redirect($_SERVER['HTTP_REFERER']);
        }

        else {

            $id_join = 'nik';
            $id_asal = 'id_kelas';
            $tabel1 = 'tb_kelas';
            $tabel2 = 'tb_guru';

            $this->data['data_kelas'] = $this->a->ambil_data_relasi($tabel1,$id_asal,$tabel2,$id_join);
            $kodemax = $this->a->no_otomatis($tabel1,$id_asal);
            $this->data['kodejadi']  = "KEL_".$kodemax;

            $this->data['list_combo'] = $this->a->list_combo($tabel2);
            $this->data['edit_combo']  = $this->a->list_combo($tabel1);
            $this->template->admin_render('back/admin/v_data_kelas',$this->data);     
        }
    }


    public function data_guru()
    {

        $nik = $this->input->post('nik');
        $tabel = 'tb_guru';

        if(isset($_POST['save']))
        {

            $data = array(
            'nik' => $nik,
            'nama_guru' => $this->input->post('nm_guru'),
            'email' => $this->input->post('email'),
            'alamat' => $this->input->post('alamat'),
            'no_hp' => $this->input->post('no_hp')
            );

            $this->a->insert_data($tabel,$data); //akses model untuk menyimpan ke database

            $datalogin = array(
            'username' => $nik,
            'password' => md5($nik),
            'level' => '2' 
            );

            $this->a->insert_data('tb_login',$datalogin); //akses model untuk menyimpan ke database

            $this->session->set_flashdata('sukses_input','Ditambahkan');
            redirect($_SERVER['HTTP_REFERER']);

        } 
        
        else if (isset($_POST['ubah'])) 
        
        {

            $data = array(
            'nik' => $nik,
            'nama_guru' => $this->input->post('nm_guru'),
            'alamat' => $this->input->post('alamat'),
            'email' => $this->input->post('email'),
            'no_hp' => $this->input->post('no_hp')
            );


            $this->a->update_data($tabel,'nik',$nik,$data); //akses model untuk menyimpan ke database


            $this->session->set_flashdata('sukses_ubah','Diubah');
            redirect($_SERVER['HTTP_REFERER']);

        }

        else if (isset($_POST['hapus'])) 
        
        {
             $this->a->hapus_data($tabel,'nik',$nik);
             $this->a->hapus_data('tb_login','username',$nik);
             $this->session->set_flashdata('sukses_hapus','Dihapus');
             redirect($_SERVER['HTTP_REFERER']);
        }

        else {
            $tbl = 'tb_guru';
            $id_tbl = 'nik';
            $this->data['data_guru'] = $this->a->ambil_data($tbl,$id_tbl);
            $this->template->admin_render('back/admin/v_data_guru',$this->data);   
        }
    }

    public function data_siswa()
    {

        $nisn = $this->input->post('nisn');
        $tabel = 'tb_siswa';

       
        if(isset($_POST['save']))
        {
            

            $a = $this->a->cek_kesamaan_data($tabel,'nisn',$nisn);
            if (!empty($a)) {

                $this->session->set_flashdata('nisn_ada','Sudah Ada');
                redirect($_SERVER['HTTP_REFERER']);
            }

            $data = array(
            'nisn' => $nisn,
            'nama_lengkap' => $this->input->post('nm_lengkap'),
            'tempat_lahir' => $this->input->post('tmpt_lahir'),
            'tanggal_lahir' => $this->input->post('tgl_lahir'),
            'alamat' => $this->input->post('alamat'),
            'no_hp' => $this->input->post('no_hp'),
            'email' => $this->input->post('email'),
            'id_kelas' => $this->input->post('id_kelas')
            );

            $this->a->insert_data($tabel,$data); //akses model untuk menyimpan ke database


            $datalogin = array(
            'username' => $nisn,
            'password' => md5($nisn),
            'level' => '3' 
            );

            $this->a->insert_data('tb_login',$datalogin); //akses model untuk menyimpan ke database

            $this->session->set_flashdata('sukses_input','Ditambahkan');
            redirect($_SERVER['HTTP_REFERER']);

        } 
        
        else if (isset($_POST['ubah'])) 
        
        {
            

            $data = array(
            'nama_lengkap' => $this->input->post('nm_lengkap'),
            'tempat_lahir' => $this->input->post('tmpt_lahir'),
            'tanggal_lahir' => $this->input->post('tgl_lahir'),
            'alamat' => $this->input->post('alamat'),
            'no_hp' => $this->input->post('no_hp'),
            'email' => $this->input->post('email'),
            'id_kelas' => $this->input->post('id_kelas')
            );

            $this->a->update_data($tabel,'nisn',$nisn,$data); //akses model untuk menyimpan ke database

         

            $this->session->set_flashdata('sukses_ubah','Diubah');
            redirect($_SERVER['HTTP_REFERER']);

        }

        else if (isset($_POST['hapus'])) 
        
        {
             $this->a->hapus_data($tabel,'nisn',$nisn);
             $this->a->hapus_data('tb_login','username',$nisn);
             $this->session->set_flashdata('sukses_hapus','Dihapus');
             redirect($_SERVER['HTTP_REFERER']);
        }

        else {

            $id_join = 'id_kelas';
            $id_asal = 'nisn';
            $tabel1 = 'tb_siswa';
            $tabel2 = 'tb_kelas';

            $this->data['data_siswa'] = $this->a->ambil_data_relasi($tabel1,$id_asal,$tabel2,$id_join);
            $this->data['list_combo'] = $this->a->list_combo($tabel2);
            $this->data['edit_combo']  = $this->a->list_combo($tabel1);
            $this->template->admin_render('back/admin/v_data_siswa',$this->data);     
        }
    }
     

public function data_login()
    {

        $id_login = $this->input->post('id');
        $tabel = 'tb_login';


        if(isset($_POST['ubah']))
        {
           
            $data = array(
            'password' => md5($this->input->post('password'))
            );

            $this->a->update_data($tabel,'id',$id_login,$data); //akses model untuk menyimpan ke database


            $this->session->set_flashdata('sukses_ubah','Diubah');
            redirect($_SERVER['HTTP_REFERER']);


        } 
        

        else if (isset($_POST['hapus'])) 
        
        {
             $this->a->hapus_data($tabel,'id',$id_login);
             $this->session->set_flashdata('sukses_hapus','Dihapus');
             redirect($_SERVER['HTTP_REFERER']);
        }

        else {

            $tbl = 'tb_login';
            $id_login = 'id';
            $this->data['data_login'] = $this->a->ambil_data($tbl,$id_login);
            $this->template->admin_render('back/admin/v_pengguna',$this->data);        
        }
    }


}