<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_Function','f');
		
	}

	public function index()
	{


		if (!empty($this->data['user_login']) && $this->data['user_login'] == '3') {

			$this->data['title'] = "Beranda Siswa";

			$this->template->admin_render('back/siswa/v_beranda',$this->data);


		}

		else if (!empty($this->data['user_login']) && $this->data['user_login'] == '2') {

			$this->data['title'] = "Beranda Guru";

			$this->template->admin_render('back/guru/v_beranda',$this->data);


		}
		

		else if (!empty($this->data['user_login']) && $this->data['user_login'] == '1') {
			
			$this->data['title'] = "Beranda Administrator";			
			
			$this->template->admin_render('back/admin/v_beranda',$this->data);

		}

		 else {
            redirect('Beranda');
        }

	}


	public function login()
	{
		if($this->session->userdata('username')) {
			redirect('beranda', 'refresh');
		}

			$valid_user = $this->f->check_credential();
			
			
			if($valid_user == FALSE)
			{
				$a = $this->session->set_flashdata("pesan", "<script type=\"text/javascript\">alert('Salah Username/Password');</script>");
				

				redirect('auth/show');
			} 
			else if ($valid_user == TRUE)
			{
				// if the username and password is a match
				$this->session->set_userdata('username', $valid_user->username);
				$this->session->set_userdata('group', $valid_user->level);
				
				$this->session->userdata('username');
				$this->session->userdata('group');

				redirect('auth/index');				
			}

			
	}

	public function show() {

		$this->template->login_render('back/v_login',$this->data);

	}


	public function logout()
	{
		$this->session->sess_destroy();
        redirect(base_url());
	}


}
