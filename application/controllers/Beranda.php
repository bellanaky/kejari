<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_Function','f');
		
	}

public function index()
	{

		$this->template->front_render('front/beranda',$this->data);


	}

	
}