<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

        /* COMMON :: ADMIN & PUBLIC */
        
        /* Data */
        $this->data['back_dir'] = $this->config->item('back_dir');
        $this->data['front_dir'] = $this->config->item('front_dir');
        $this->data['assets_dir'] = $this->config->item('assets_dir');
        $this->data['avatar_dir'] = $this->config->item('avatar_dir');

        $this->data['user_login'] = $this->session->userdata('group'); 
        $this->data['username_login'] = $this->session->userdata('username'); 

        $this->data['title'] = $this->config->item('page_title');
        

    }
}