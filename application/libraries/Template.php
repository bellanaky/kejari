<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template {

    protected $CI;

    public function __construct()
    {	
		$this->CI =& get_instance();
    }


    public function auth_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']  = $this->CI->load->view('auth/_templates/header', $data, TRUE);
            $data['notices']  = $this->CI->load->view('auth/_templates/notices', $data, TRUE);
            $this->template['content'] = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']  = $this->CI->load->view('auth/_templates/footer', $data, TRUE);

            return $this->CI->load->view('auth/_templates/template', $this->template);
        }
	}

       public function admin_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']          = $this->CI->load->view('back/_templates/v_header', $data, TRUE);
            $this->template['sidebar']         = $this->CI->load->view('back/_templates/v_sidebar', $data, TRUE);
            $this->template['notices']         = $this->CI->load->view('back/_templates/v_notices', $data, TRUE);
            $this->template['content']         = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']          = $this->CI->load->view('back/_templates/v_footer', $data, TRUE);

            return $this->CI->load->view('back/_templates/v_template', $this->template);
        }
    }

    public function login_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']          = $this->CI->load->view('back/_templates/v_header', $data, TRUE);
            $this->template['content']         = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']          = $this->CI->load->view('back/_templates/v_footer_login', $data, TRUE);

            return $this->CI->load->view('back/_templates/v_template', $this->template);
        }
    }

    public function siswa_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']          = $this->CI->load->view('public/_templates/header', $data, TRUE);
            $this->template['main_header']     = $this->CI->load->view('public/_templates/main_header', $data, TRUE);
            $data['notices']         = $this->CI->load->view('public/_templates/notices', $data, TRUE);
            $this->template['main_sidebar']    = $this->CI->load->view('public/_templates/main_sidebar', $data, TRUE);
            $this->template['content']         = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']          = $this->CI->load->view('public/_templates/footer', $data, TRUE);

            return $this->CI->load->view('public/_templates/template', $this->template);
        }
    }



    public function front_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']  = $this->CI->load->view('front/_templates/header', $data, TRUE);
            $this->template['content'] = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']  = $this->CI->load->view('front/_templates/footer', $data, TRUE);

            return $this->CI->load->view('front/_templates/template', $this->template);
        }
    }

}