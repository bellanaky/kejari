<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_Function extends CI_Model {


	public function check_credential()
		{
			$username = set_value('username');
			$password = set_value('password');
			
			$hasil = $this->db->where('username', $username)
							  ->where('password', md5($password))
							  ->limit(1)
							  ->get('tb_login');
			
			if($hasil->num_rows() > 0){
				return $hasil->row();
			} else {
				return array();
			}
		}


	// ============ Tampil Data ================ 

	public function ambil_data($tbl,$id_tbl){

    $query = $this->db->select('*')
     ->from($tbl)  
     ->order_by($tbl.'.'.$id_tbl,'desc')
     ->get();
     return $query->result();

 	 }

  // public function ambil_data_relasi($tbl,$id_tbl,$tbl2,$id_tbl2){
 	public function ambil_data_relasi($tbl,$id_tbl,$tbl2,$id_tbl2){

   	 $query = $this->db->select('*')
     ->from($tbl.' '.'as'.' '.'tb1')
     ->join($tbl2.' '.'as'.' '.'tb2','tb2'.'.'.$id_tbl2.'='.'tb1'.'.'.$id_tbl2)
     ->order_by('tb1'.'.'.$id_tbl,'desc')
     ->get();
     return $query->result();

 	 }

	// ========== EndTampil Data =============== 


	function insert_data($tbl, $data){
     $this->db->insert($tbl, $data);
     return TRUE;
    }

    function update_data($tbl,$cari, $id, $data)
    {
      $this->db->where($cari, $id);
      $this->db->update($tbl, $data);
      return array();
    }

    public function hapus_data($tbl, $cari, $id)
    {
      $this->db->where($cari, $id);
      $this->db->delete($tbl);
      if ($this->db->affected_rows() == 1) {
        return TRUE;
        }
      return FALSE;
    }


     public function no_otomatis($table,$id)  {
     		
		$this->db->select('Right('.$table.'.'.$id.',5) as kode ',false);
	    $this->db->order_by($id, 'desc');
	    $this->db->limit(1);
	    $query = $this->db->get($table);
	    if($query->num_rows()<>0){
	        $data = $query->row();
	        $kode = intval($data->kode)+1;
	    }else{
	        $kode = 1;

	    }
	    $kodemax = str_pad($kode,5,"0",STR_PAD_LEFT);
	    return $kodemax;
	 }

	public function list_combo($tbl) {
   	$query = $this->db->select('*')
     ->from($tbl)  
     ->get();
     return $query->result();
  }

  public function cek_kesamaan_data($tbl,$yangdicari,$idcari) {
    $query = $this->db->select('*')
     ->from($tbl)  
     ->where($yangdicari,$idcari)  
     ->get();
     return $query->result();
  }

// ========= Total Data ===========

  var $tb_guru = 'tb_guru';
  var $tb_siswa = 'tb_siswa';
  var $tb_mapel = 'tb_mapel';
  var $tb_kelas = 'tb_kelas';

  public function getDataTotalSiswa(){
    $data = $this->db->get($this->tb_siswa);
    $this->db->from($this->tb_siswa);
    $query = $this->db->get();
    return $query;
  }

 public function getDataTotalGuru(){
    $data = $this->db->get($this->tb_guru);
    $this->db->from($this->tb_guru);
    $query = $this->db->get();
    return $query;
  }

  public function getDataTotalMapel(){
    $data = $this->db->get($this->tb_mapel);
    $this->db->from($this->tb_mapel);
    $query = $this->db->get();
    return $query;
  }

   public function getDataTotalKelas(){
    $data = $this->db->get($this->tb_kelas);
    $this->db->from($this->tb_kelas);
    $query = $this->db->get();
    return $query;
  }

// ========= End Total Data ===========


}