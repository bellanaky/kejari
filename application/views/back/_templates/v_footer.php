  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y')?> || SMA XAVERIUS BANDAR LAMPUNG</strong>
    <div class="float-right d-none d-sm-inline-block">
      <b> Versi </b> 1.0
    </div>
  </footer>

<!-- Fungsi Trim Textarea -->




<!-- jQuery -->
<script src="<?php echo site_url($back_dir);?>/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo site_url($back_dir);?>/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>

<!-- Fungsi Trim Textarea -->

<script type="text/javascript">
$('document').ready(function()
{
$('textarea').each(function(){
$(this).val($(this).val().trim());
}
);
});
</script>

<!-- Bootstrap 4 -->
<script src="<?php echo site_url($back_dir);?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo site_url($back_dir);?>/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo site_url($back_dir);?>/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?php echo site_url($back_dir);?>/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo site_url($back_dir);?>/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo site_url($back_dir);?>/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo site_url($back_dir);?>/plugins/moment/moment.min.js"></script>
<script src="<?php echo site_url($back_dir);?>/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo site_url($back_dir);?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo site_url($back_dir);?>/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo site_url($back_dir);?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url($back_dir);?>/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo site_url($back_dir);?>/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo site_url($back_dir);?>/dist/js/demo.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="<?php echo site_url($back_dir);?>/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- Bootstrap Switch -->
<script src="<?php echo site_url($back_dir);?>/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>



<!-- DataTables -->
<script src="<?php echo site_url($back_dir);?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo site_url($back_dir);?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo site_url($back_dir);?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo site_url($back_dir);?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- Select2 -->
<script src="<?php echo site_url($back_dir);?>/plugins/select2/js/select2.full.min.js"></script>


<script>
  $(function () {

    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "responsive": true,
      "language": {
      "url": "<?php echo base_url($back_dir);?>/plugins/datatables/indonesian.json"
      }
    });
  });

</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

  })
</script>


</body>
</html>
