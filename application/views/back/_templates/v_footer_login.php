<!-- jQuery -->
<script src="<?php echo site_url($back_dir);?>/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo site_url($back_dir);?>/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo site_url($back_dir);?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo site_url($back_dir);?>/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo site_url($back_dir);?>/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?php echo site_url($back_dir);?>/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo site_url($back_dir);?>/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo site_url($back_dir);?>/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo site_url($back_dir);?>/plugins/moment/moment.min.js"></script>
<script src="<?php echo site_url($back_dir);?>/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo site_url($back_dir);?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo site_url($back_dir);?>/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo site_url($back_dir);?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url($back_dir);?>/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo site_url($back_dir);?>/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo site_url($back_dir);?>/dist/js/demo.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="<?php echo site_url($back_dir);?>/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- Bootstrap Switch -->
<script src="<?php echo site_url($back_dir);?>/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>