

<?php if ($this->session->flashdata('sukses_input')){ ?>
    <script>
        swal({
            title: "Sukses",
            text: "Berhasil Simpan Data",
            showConfirmButton : true,
            type : "success",
            icon: "success"
        });
    </script>
<?php } ?>


<?php if ($this->session->flashdata('sukses_ubah')){ ?>
    <script>
        swal({
            title: "Sukses",
            text: "Berhasil Ubah Data",
            showConfirmButton : true,
            type : "success",
            icon: "success"
        });
    </script>
<?php } ?>


<?php if ($this->session->flashdata('sukses_hapus')){ ?>
    <script>
        swal({
            title: "Sukses",
            text: "Berhasil Hapus Data",
            showConfirmButton : true,
            type : "success",
            icon: "success"
        });
    </script>
<?php } ?>


<?php if ($this->session->flashdata('nisn_ada')){ ?>
    <script>
        swal({
            title: "Perhatian",
            text: "NISN Sudah Ada, Silahkan Periksa Kembali",
            showConfirmButton : true,
            type : "warning",
            icon: "warning"
        });
    </script>
<?php } ?>


