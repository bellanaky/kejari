

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
      <div class="row">

        <!-- Ketik Koding Disini -->

         <section class="col-lg-12 connectedSortable">
         

            <div class="card">
            <div class="card-header">
              <h3 class="card-title">Kelola Data Guru</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <p align="left" style="margin-bottom: 30px">
              <a href="#" data-toggle="modal" data-target="#modal-tambah">
              <button class="btn btn-success"> <i class="fa fa-plus"> </i> &nbsp; Tambah Data </button>
              </a>
              </p>

              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Ubah</th> 
                  <th>Hapus</th> 
                  <th>Nama Guru</th>
                  <th>No HP</th>
                  <th>Email</th>
                  <th>Alamat</th>
                </tr>
                </thead>
                <tbody>

                 <?php if(is_array($data_guru)){ ?>
                 <?php $no = 1;?>
                 <?php foreach($data_guru as $dt) : ?>

                  <tr>
                  <td><?php echo $no?></th>
                  <td>  
                    <a href="#" data-toggle="modal" data-target="#modal-ubah<?php echo $dt->nik;?>">
                     <button  type="button" class="btn bg-gradient-primary btn-sm" title="Ubah Data"><i class="fa fa-edit"> </i></button>
                    </a> 
                  </td>
                  <td> 
                  <a href="#" data-toggle="modal" data-target="#modal-hapus">
                  <button  type="button" class="btn bg-gradient-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"> </i></button>
                  </a> 

                  </td>
                  <td><?php echo $dt->nama_guru?></th>
                  <td><?php echo $dt->no_hp?></th>
                  <td><?php echo $dt->email?></th>
                  <td><?php echo $dt->alamat?></th>
                
                  </tr>

                 <?php $no++; ?>
                 <?php endforeach; ?>
                 <?php } ?>


              </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>

         
          </section>

      </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Modal Tambah Data -->

  <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>


            <div class="modal-body">
            
            <form action="<?php echo base_url('admin/data_guru'); ?>" method="POST" enctype="multipart/form-data">

              <div class="row">
                    <div class="col-sm-6">
                     <div class="form-group">
                    <label for="exampleInputEmail1">Nomor Induk Karyawan</label>
                    <input type="text" name="nik" class="form-control"  placeholder="NIK">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Nama Guru</label>
                          <input type="text" name="nm_guru" class="form-control"  placeholder="Input Nama Guru">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Email</label>
                          <input type="text" name="email" class="form-control"  placeholder="Input Email">
                    </div>


                  
                    </div>
                    <div class="col-sm-6">
                    
                     <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <textarea class="form-control" name="alamat"></textarea>
                    </div>

                  
                    <div class="form-group">
                          <label for="exampleInputEmail1">No HP</label>
                          <input type="text" name="no_hp" class="form-control"  placeholder="Input No HP">
                    </div>

                  
                    </div>

                  </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-success"  name="save" > <i class="fa fa-plus"> </i> &nbsp;Tambah Data</button>
          </form>
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>


          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


<!-- Modal Ubah Data -->


 <?php if(is_array($data_guru)){ ?>
 <?php foreach($data_guru as $dt) : ?>


  <div class="modal fade" id="modal-ubah<?php echo $dt->nik;?>">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Ubah Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>


            <div class="modal-body">
            
            <form action="<?php echo base_url('admin/data_guru'); ?>" method="POST" enctype="multipart/form-data">

              <div class="row">
                    <div class="col-sm-6">
                     <div class="form-group">
                    <label for="exampleInputEmail1">Nomor Induk Karyawan</label>
                    <input type="text" name="nik" class="form-control"  value="<?php echo $dt->nik?>" readonly="true">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Nama Guru</label>
                          <input type="text" name="nm_guru" class="form-control" value="<?php echo $dt->nama_guru?>">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Email</label>
                          <input type="text" name="email" class="form-control"  value="<?php echo $dt->email?>">
                    </div>


                  
                    </div>
                    <div class="col-sm-6">
                    
                     <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <textarea class="form-control" name="alamat"> <?php echo $dt->alamat?></textarea>
                    </div>

                  
                    <div class="form-group">
                          <label for="exampleInputEmail1">No HP</label>
                          <input type="text" name="no_hp" class="form-control"  value="<?php echo $dt->no_hp?>">
                    </div>

                  
                    </div>

                  </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary"  name="ubah" > <i class="fa fa-edit"> </i> &nbsp;Ubah Data</button>
          </form>
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>


          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


   <?php endforeach; ?>
   <?php } ?>


<<!-- Modal Hapus Data -->


     <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Hapus Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Yakin akan menghapus data ini?</p>
            </div>
            <div class="modal-footer justify-content-between">

              <form action="<?php echo base_url('admin/data_guru'); ?>" method="POST" enctype="multipart/form-data">
              <input type="hidden" name="nik" class="form-control" value="<?php echo $dt->nik;?>">
              <button type="submit" class="btn btn-danger" name="hapus"> <i class="fa fa-trash"> </i> &nbsp; Hapus Data</button>
              </form>
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


