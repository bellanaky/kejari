

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
      <div class="row">

        <!-- Ketik Koding Disini -->

         <section class="col-lg-12 connectedSortable">
         

            <div class="card">
            <div class="card-header">
              <h3 class="card-title">Kelola Data Mapel</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <p align="left" style="margin-bottom: 30px">
              <a href="#" data-toggle="modal" data-target="#modal-tambah">
              <button class="btn btn-success"> <i class="fa fa-plus"> </i> &nbsp; Tambah Data </button>
              </a>
              </p>

              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Ubah</th> 
                  <th>Hapus</th> 
                  <th>Nama Mata Pelajaran</th>
                  <th>Tahun Ajaran</th>
                </tr>
                </thead>
                <tbody>

                 <?php if(is_array($data_mapel)){ ?>
                 <?php $no = 1;?>
                 <?php foreach($data_mapel as $dt) : ?>

                  <tr>
                  <td><?php echo $no?></th>
                  <td> 
                      <a href="#" data-toggle="modal" data-target="#modal-ubah<?php echo $dt->id_mapel;?>">
                      <button  type="button" class="btn bg-gradient-primary btn-sm" title="Ubah Data"><i class="fa fa-edit"> </i></button>
                      </a>
                  </td>

                  <td> 
                      <a href="#" data-toggle="modal" data-target="#modal-hapus">
                      <button  type="button" class="btn bg-gradient-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"> </i></button>
                      </a> 
                  </td>
                  <td><?php echo $dt->nama_mapel?></th>
                  <td><?php echo $dt->tahun_ajaran?></th>
                  
                  </tr>

                 <?php $no++; ?>
                 <?php endforeach; ?>
                 <?php } ?>


              </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>

         
          </section>

      </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Modal Tambah Data -->

  <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>


            <div class="modal-body">
            
            <form action="<?php echo base_url('admin/data_mapel'); ?>" method="POST" enctype="multipart/form-data">

              <input type="hidden" name="id_mapel" class="form-control" value="<?php echo $kodejadi;?>">

              <div class="form-group">
                    <label for="exampleInputEmail1">Nama Mapel</label>
                    <input type="text" name="nm_mapel" class="form-control"  placeholder="Input Mapel">
              </div>
              <div class="form-group">
                    <label for="exampleInputEmail1">Tahun Ajaran</label>
                    <input type="text" name="th_ajaran" class="form-control"  placeholder="Input Tahun Ajaran">
              </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-success"  name="save" > <i class="fa fa-plus"> </i> &nbsp;Tambah Data</button>
          </form>
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>


          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


<!-- Modal Ubah Data -->

   <?php if(is_array($data_mapel)){ ?>
   <?php foreach($data_mapel as $dt) : ?>


  <div class="modal fade" id="modal-ubah<?php echo $dt->id_mapel;?>">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Ubah Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>


            <div class="modal-body">
            
            <form action="<?php echo base_url('admin/data_mapel'); ?>" method="POST" enctype="multipart/form-data">

              <input type="hidden" name="id_mapel" class="form-control" value="<?php echo $dt->id_mapel;?>">

              <div class="form-group">
                    <label for="exampleInputEmail1">Nama Mapel</label>
                    <input type="text" name="nm_mapel" class="form-control"  value="<?php echo $dt->nama_mapel?>">
              </div>
              <div class="form-group">
                    <label for="exampleInputEmail1">Tahun Ajaran</label>
                    <input type="text" name="th_ajaran" class="form-control"  value="<?php echo $dt->tahun_ajaran?>">
              </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary"  name="ubah" > <i class="fa fa-edit"> </i> &nbsp;Ubah Data</button>
          </form>
             <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>

          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



   <?php endforeach; ?>
   <?php } ?>


<!-- Modal Hapus Data -->


     <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Hapus Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Yakin akan menghapus data ini?</p>
            </div>
            <div class="modal-footer justify-content-between">


              <form action="<?php echo base_url('admin/data_mapel'); ?>" method="POST" enctype="multipart/form-data">
              <input type="hidden" name="id_mapel" class="form-control" value="<?php echo $dt->id_mapel;?>">
              <button type="submit" class="btn btn-danger" name="hapus"> <i class="fa fa-trash"> </i> &nbsp; Hapus Data</button>
              </form>
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->