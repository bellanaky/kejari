

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
      <div class="row">

        <!-- Ketik Koding Disini -->

         <section class="col-lg-12 connectedSortable">
         

            <div class="card">
            <div class="card-header">
              <h3 class="card-title">Kelola Data Siswa</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <p align="left" style="margin-bottom: 30px">
              <a href="#" data-toggle="modal" data-target="#modal-tambah">
              <button class="btn btn-success"> <i class="fa fa-plus"> </i> &nbsp; Tambah Data </button>
              </a>
              </p>

              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Ubah</th> 
                  <th>Hapus</th> 
                  <th>NISN</th>
                  <th>Nama Lengkap</th>
                  <th>Tempat/Tgl Lahirr</th>
                  <th>Alamat</th>
                  <th>No Handphone</th>
                  <th>Email</th>
                  <th>Kelas</th>
                </tr>
                </thead>
                <tbody>

                 <?php if(is_array($data_siswa)){ ?>
                 <?php $no = 1;?>
                 <?php foreach($data_siswa as $dt) : ?>

                  <tr>
                  <td><?php echo $no?></td>
                   <td> 
                    <a href="#" data-toggle="modal" data-target="#modal-ubah<?php echo $dt->nisn;?>">
                    <button  type="button" class="btn bg-gradient-primary btn-sm" title="Ubah Data"><i class="fa fa-edit"> </i></button>
                    </a> 
                  </td>

                  <td>
                    <a href="#" data-toggle="modal" data-target="#modal-hapus">
                    <button  type="button" class="btn bg-gradient-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"> </i></button>
                    </a> 
                  </td>
                  <td><?php echo $dt->nisn?></td>
                  <td><?php echo $dt->nama_lengkap?></td>
                  <td><?php echo $dt->tempat_lahir?>, <?php echo $dt->tanggal_lahir?></td>
                  <td><?php echo $dt->alamat?></td>
                  <td><?php echo $dt->no_hp?></td>
                  <td><?php echo $dt->email?></td>
                  <td><?php echo $dt->nama_kelas?></td>
                 
                  </tr>

                 <?php $no++; ?>
                 <?php endforeach; ?>
                 <?php } ?>


              </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>

         
          </section>

      </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Modal Tambah Data -->

  <div class="modal fade" id="modal-tambah">

    <div class="modal-dialog modal-lg">
          
    <div class="modal-content">
          
        <div class="modal-header">
          <h4 class="modal-title">Tambah Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>


        <div class="modal-body">
            

            <form action="<?php echo base_url('admin/data_siswa'); ?>" method="POST" enctype="multipart/form-data">

               <div class="row">
                    <div class="col-sm-6">
                     <div class="form-group">
                    <label for="exampleInputEmail1">NISN</label>
                    <input type="text" name="nisn" class="form-control"  placeholder="NISN">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Nama Lengkap</label>
                          <input type="text" name="nm_lengkap" class="form-control"  placeholder="Input Nama Lengkap">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Tempat Lahir</label>
                          <input type="text" name="tmpt_lahir" class="form-control"  placeholder="Input Tempat Lahir">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Tanggal Lahir</label>
                          <input type="date" name="tgl_lahir" class="form-control"  >
                    </div>
                    
                    </div>
                    <div class="col-sm-6">
                    
                    <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <textarea class="form-control" name="alamat"></textarea>
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">No Handphone</label>
                          <input type="text" name="no_hp" class="form-control"  placeholder="Input Nomor Handphone">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Email</label>
                          <input type="text" name="email" class="form-control"  placeholder="Input Email">
                    </div>

                    <div class="form-group">
                          <label>Pilih Kelas</label>
                          <select class="form-control select2bs4" style="width: 100%;" name="id_kelas">
                          <option value="" selected="selected" disabled> Pilih Kelas</option> 
                          <?php 
                          foreach($list_combo as $row)
                          { 
                          echo '
                          <option value="'.$row->id_kelas.'">'.$row->nama_kelas.'</option>';
                          }
                          ?>
                        </select>
                    </div>

                    </div>

                  </div>


        </div>
            
        <div class="modal-footer justify-content-between">
          <button type="submit" class="btn btn-success"  name="save" > <i class="fa fa-plus"> </i> &nbsp;Tambah Data</button>
          </form>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>  
    

    </div>

    </div>

  </div>

<!-- /.modal -->



<!-- Modal Ubah Data -->

 <?php if(is_array($data_siswa)){ ?>
 <?php foreach($data_siswa as $dt) : ?>


<div class="modal fade" id="modal-ubah<?php echo $dt->nisn;?>">

    <div class="modal-dialog modal-lg">
          
    <div class="modal-content">
          
        <div class="modal-header">
          <h4 class="modal-title">Ubah Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>


        <div class="modal-body">
            

            <form action="<?php echo base_url('admin/data_siswa'); ?>" method="POST" enctype="multipart/form-data">

               <div class="row">
                    <div class="col-sm-6">
                     <div class="form-group">
                    <label for="exampleInputEmail1">NISN </label>
                    <input type="text" name="nisn" class="form-control"  value="<?php echo $dt->nisn?>" readonly = "true">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Nama Lengkap</label>
                          <input type="text" name="nm_lengkap" class="form-control"  value="<?php echo $dt->nama_lengkap?>">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Tempat Lahir</label>
                          <input type="text" name="tmpt_lahir" class="form-control"  value="<?php echo $dt->tempat_lahir?>">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Tanggal Lahir</label>
                          <input type="date" name="tgl_lahir" class="form-control"  value="<?php echo $dt->tanggal_lahir?>" >
                    </div>
                    
                    </div>
                    <div class="col-sm-6">
                    
                     <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <textarea class="form-control" name="alamat"> <?php echo $dt->alamat?></textarea>
                    </div>
                    

                    <div class="form-group">
                          <label for="exampleInputEmail1">No Handphone</label>
                          <input type="text" name="no_hp" class="form-control"   value="<?php echo $dt->no_hp?>">
                    </div>

                    <div class="form-group">
                          <label for="exampleInputEmail1">Email</label>
                          <input type="text" name="email" class="form-control"  value="<?php echo $dt->email?>">
                    </div>

                    <div class="form-group">
                          <label>Pilih Kelas</label>
                          <select class="form-control select2bs4" style="width: 100%;" name="id_kelas">
                          <option value="" selected="selected" disabled> Pilih Kelas</option> 
                          <?php foreach($list_combo as $dta) : ?>
                          <?php if($dt->id_kelas != $dta->id_kelas){ ?>
                          <option value="<?php echo $dta->id_kelas  ?>"><?php echo $dta->nama_kelas?> </option>
                          <?php } else {?>
                          <option  <?php if($dta->id_kelas == $dta->id_kelas){ echo 'selected="selected"';
                          } ?>
                          value="<?php echo $dta->id_kelas  ?>"><?php echo $dta->nama_kelas?> </option>
                          <?php }?>
                          <?php endforeach?>


                        </select>
                    </div>

                    </div>

                  </div>


        </div>
            
        <div class="modal-footer justify-content-between">
          <button type="submit" class="btn btn-primary"  name="ubah" > <i class="fa fa-edit"> </i> &nbsp;Ubah Data</button>
          </form>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        </div>  
    

    </div>

    </div>

  </div>



   <?php endforeach; ?>
   <?php } ?>

<!-- /.modal -->


<!-- Modal Hapus Data -->


     <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Hapus Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Yakin akan menghapus data ini?</p>
            </div>
            <div class="modal-footer justify-content-between">

              <form action="<?php echo base_url('admin/data_siswa'); ?>" method="POST" enctype="multipart/form-data">
              <input type="hidden" name="nisn" class="form-control" value="<?php echo $dt->nisn;?>">
              <button type="submit" class="btn btn-danger" name="hapus"> <i class="fa fa-trash"> </i> &nbsp; Hapus Data</button>
              </form>
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


