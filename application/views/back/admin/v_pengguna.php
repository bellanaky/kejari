

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
      <div class="row">

        <!-- Ketik Koding Disini -->

         <section class="col-lg-12 connectedSortable">
         

            <div class="card">
            <div class="card-header">
              <h3 class="card-title">Kelola Data Pengguna</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <p align="left" style="margin-bottom: 30px">
              
              </p>

              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Ubah Password</th>
                  <th>Username</th> 
                </tr>
                </thead>
                <tbody>

                 <?php if(is_array($data_login)){ ?>
                 <?php $no = 1;?>
                 <?php foreach($data_login as $dt) : ?>

                  <tr>
                  <td><?php echo $no?></th>
                  <td>  
                    <a href="#" data-toggle="modal" data-target="#modal-ubah<?php echo $dt->id;?>">
                     <button  type="button" class="btn bg-gradient-primary btn-sm" title="Ubah Data"><i class="fa fa-edit"> </i></button>
                    </a> 
                  </td>
                  <td><?php echo $dt->username?></th>                
                  </tr>

                 <?php $no++; ?>
                 <?php endforeach; ?>
                 <?php } ?>


              </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>

         
          </section>

      </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Modal Ubah Password -->


 <?php if(is_array($data_login)){ ?>
 <?php foreach($data_login as $dt) : ?>


  <div class="modal fade" id="modal-ubah<?php echo $dt->id;?>">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Ubah Password</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>


            <div class="modal-body">
            
            <form action="<?php echo base_url('admin/data_login'); ?>" method="POST" enctype="multipart/form-data">

              <div class="row">

                  <input type="hidden" value="<?=$dt->id?>" name="id">

                    <div class="col-sm-12">

                    <div class="form-group">
                          <label for="exampleInputEmail1">Password</label>
                          <input type="text" name="password" class="form-control" placeholder="Ubah Password">
                    </div>
                  
                    </div>
                   

                  </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary"  name="ubah" > <i class="fa fa-edit"> </i> &nbsp;Ubah Data</button>
          </form>
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>


          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


   <?php endforeach; ?>
   <?php } ?>


<<!-- Modal Hapus Data -->


     <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Hapus Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Yakin akan menghapus data ini?</p>
            </div>
            <div class="modal-footer justify-content-between">

              <form action="<?php echo base_url('admin/data_login'); ?>" method="POST" enctype="multipart/form-data">
              <input type="hidden" name="id" class="form-control" value="<?php echo $dt->id;?>">
              <button type="submit" class="btn btn-danger" name="hapus"> <i class="fa fa-trash"> </i> &nbsp; Hapus Data</button>
              </form>
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


