

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
      <div class="row">

        <!-- Ketik Koding Disini -->

         <section class="col-lg-12 connectedSortable">
         

            <div class="card">
            <div class="card-header">
              <h3 class="card-title">Kelola Data Materi</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <p align="left" style="margin-bottom: 30px">
              <a href="#" data-toggle="modal" data-target="#modal-tambah">
              <button class="btn btn-success"> <i class="fa fa-plus"> </i> &nbsp; Tambah Data </button>
              </a>
              </p>

              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Ubah</th> 
                  <th>Hapus</th> 
                  <th>Nama Materi</th>
                  <th>File PDF</th>
                  <th>Tanggal Upload</th>
                  <th>Mata Pelajaran</th>
                </tr>
                </thead>
                <tbody>

                 <?php if(is_array($data_materi)){ ?>
                 <?php $no = 1;?>
                 <?php foreach($data_materi as $dt) : ?>

                  <tr>
                  <td><?php echo $no?></td>
                  <td>
                      <a href="#" data-toggle="modal" data-target="#modal-ubah<?php echo $dt->id_materi;?>">
                      <button  type="button" class="btn bg-gradient-primary btn-sm" title="Ubah Data"><i class="fa fa-edit"> </i></button>
                      </a> 
                  </td>
                  <td> 
                      <a href="#" data-toggle="modal" data-target="#modal-hapus">
                      <button  type="button" class="btn bg-gradient-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"> </i></button>
                      </a> 
                  </td>
                  <td><?php echo $dt->nama_materi?></td>
                  <td><?php echo $dt->file_pdf?></td>
                  <td><?php echo tgl_upload($dt->tgl_upload)?></td>
                  <td><?php echo $dt->nm_mapel?></td>
               
                  </tr>

                 <?php $no++; ?>
                 <?php endforeach; ?>
                 <?php } ?>


              </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>

         
          </section>

      </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Modal Tambah Data -->

  <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>


            <div class="modal-body">
            
            <form action="<?php echo base_url('admin/data_materi'); ?>" method="POST" enctype="multipart/form-data">

              <input type="hidden" name="id_materi" class="form-control" value="<?php echo $kodejadi;?>">

              <div class="row">
                    <div class="col-sm-6">
                    
                    <div class="form-group">
                    <label for="exampleInputEmail1">Nama Materi</label>
                    <input type="text" name="nama_materi" class="form-control"  placeholder="Nama Materi">
                    </div>


                 
                    <div class="form-group">
                    <label for="exampleInputEmail1">Keterangan</label>
                    <textarea class="form-control" name="keterangan"></textarea>
                    </div>

                   
                  
                    </div>
                    <div class="col-sm-6">
                 
                  <div class="form-group">
                    <label for="exampleInputFile">File PDF</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Pilih File</label>
                      </div>
                     
                    </div>
                  </div>

                  
                   <div class="form-group">
                    <label>Pilih Kelas</label>
                    <select class="form-control select2bs4" style="width: 100%;" name="id_kelas">
                    <option value="" selected="selected" disabled> Pilih Mata Pelajaran</option> 
                    <?php 
                    foreach($list_combo as $row)
                    { 
                    echo '
                    <option value="'.$row->id_mapel.'">'.$row->nama_mapel.'</option>';
                    }
                    ?>
                  </select>
                  </div>


                  
                    </div>

                  </div>
              

            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-success"  name="save" > <i class="fa fa-plus"> </i> &nbsp;Tambah Data</button>
          </form>
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>


          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


<!-- Modal Ubah Data -->

 <?php if(is_array($data_materi)){ ?>
 <?php foreach($data_materi as $dt) : ?>


  <div class="modal fade" id="modal-ubah<?php echo $dt->id_kelas;?>">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Ubah Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>


            <div class="modal-body">
            
            <form action="<?php echo base_url('guru/data_materi'); ?>" method="POST" enctype="multipart/form-data">

              <input type="hidden" name="id_kelas" class="form-control" value="<?php echo $dt->id_kelas;?>">

              <div class="form-group">
                    <label for="exampleInputEmail1">Nama Kelas</label>
                    <input type="text" name="nm_kelas" class="form-control"  value="<?php echo $dt->nama_kelas?>">
              </div>
              
               <div class="form-group">
                    <label>Pilih Nama Wali Kelas</label>
                    <select class="form-control select2bs4" style="width: 100%;" name="id_guru">
                    <option value="" selected="selected" disabled> Pilih Nama</option> 
                
                      <?php foreach($list_combo as $dta) : ?>
                      <?php if($dt->nik != $dta->nik){ ?>
                      <option value="<?php echo $dta->nik  ?>"><?php echo $dta->nama_guru?> </option>
                      <?php } else {?>
                      <option  <?php if($dta->nik == $dta->nik){ echo 'selected="selected"';
                      } ?>
                      value="<?php echo $dta->nik  ?>"><?php echo $dta->nama_guru?> </option>
                      <?php }?>
                      <?php endforeach?>

                  </select>
              </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary"  name="ubah" > <i class="fa fa-edit"> </i> &nbsp;Ubah Data</button>
          </form>
             <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>

          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



   <?php endforeach; ?>
   <?php } ?>


<!-- Modal Hapus Data -->


     <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Hapus Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Yakin akan menghapus data ini?</p>
            </div>
            <div class="modal-footer justify-content-between">

              <form action="<?php echo base_url('guru/data_materi'); ?>" method="POST" enctype="multipart/form-data">
              <input type="hidden" name="id_kelas" class="form-control" value="<?php echo $dt->id_kelas;?>">
              <button type="submit" class="btn btn-danger" name="hapus"> <i class="fa fa-trash"> </i> &nbsp; Hapus Data</button>
              </form>
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


