   <!-- Footer Section Begin -->
   <br>
   <br>
    <footer class="footer set-bg " data-setbg="<?php echo site_url($front_dir);?>/img/footer-bg.jpg">
        <div class="container">
            <div class="row">
             
            </div>
        </div>
        <div class="container">
            <div class="footer__content">
                <div class="row">
                    <div class="col-lg-5 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__logo">
                                <a href="#"><img src="<?php echo site_url($front_dir);?>/img/logo_white.png" alt=""></a>
                            </div>
                            <h4>Telephone : (0727) 321102</h4>
                            <ul>
                            <li>Email : admin@kejari-lampungselatan.go.id</li>
                            </ul>
                            <div class="footer__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-youtube-play"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-1 col-md-5 offset-md-1 col-sm-6">
                        <div class="footer__widget">
                            <br>
                            <h4>Link Kejaksaan Negeri Lainnya</h4>
                            <ul>
                                <li><a href="#">Kejaksaan Negeri Republik Indonesia</a></li>
                                <li><a href="#">Kejaksaan Negeri Bandar Lampung</a></li>
                                <li><a href="#">Kejaksaan Negeri Lampung Selatan</a></li>
                              
                            </ul>
                           
                        </div>
                    </div>
                 
                </div>
            </div>
            <div class="footer__copyright">
                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <div class="footer__copyright__text">
                            <p>Hak Cipta &copy; <script>document.write(new Date().getFullYear());</script> Dibuat Oleh <a > Kejaksaan Negeri Lampung Selatan</a></p>
                        </div>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                    
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="<?php echo site_url($front_dir);?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo site_url($front_dir);?>/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url($front_dir);?>/js/jquery.nice-select.min.js"></script>
    <script src="<?php echo site_url($front_dir);?>/js/jquery-ui.min.js"></script>
    <script src="<?php echo site_url($front_dir);?>/js/jquery.slicknav.js"></script>
    <script src="<?php echo site_url($front_dir);?>/js/owl.carousel.min.js"></script>
    <script src="<?php echo site_url($front_dir);?>/js/main.js"></script>