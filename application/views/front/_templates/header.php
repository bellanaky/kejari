<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Hiroto Template">
    <meta name="keywords" content="Hiroto, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title;?></title>

    <!-- Css Styles -->
    <link rel="stylesheet" href="<?php echo site_url($front_dir);?>/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo site_url($front_dir);?>/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo site_url($front_dir);?>/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo site_url($front_dir);?>/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="<?php echo site_url($front_dir);?>/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo site_url($front_dir);?>/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo site_url($front_dir);?>/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo site_url($front_dir);?>/css/style.css" type="text/css">
</head>


<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Offcanvas Menu Begin -->
    <div class="offcanvas-menu-overlay"></div>
    <div class="offcanvas-menu-wrapper">
        <div class="offcanvas__logo">
            <a href="./index.html"><img src="<?php echo site_url($front_dir)?>/img/logo_white.png" alt=""></a>
        </div>
        <div id="mobile-menu-wrap"></div>
      
        <div class="offcanvas__widget">
            <ul>
                <li><i class="fa fa-phone"> </i>  (0727) 321102</li>
                <li><i class="fa fa-envelope"> </i>  admin@kejari-lampungselatan.go.id</li>
            </ul>
        </div>
      
    </div>
    <!-- Offcanvas Menu End -->

    <!-- Header Section Begin -->
    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <ul class="header__top__widget">
                            <li><a href="./index.html"><img src="<?php echo site_url($front_dir)?>/img/logo_awal.png" alt="" width="200px" > </a> </li>
                            <li>Website Resmi Kejaksaan Lampung Selatan</li>
                        </ul>
                    </div>
                    <div class="col-lg-5">
                        <div class="header__top__right">
                            <div class="header__top__auth">
                                <ul>
                                    <li><a href="#"><i class="fa fa-phone"> </i>  &nbsp;(0727) 321102</a></li>
                                    <li><a href="#"><i class="fa fa-envelope"> </i>  &nbsp; admin@kejari-lampungselatan.go.id</a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header__nav__option">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="header__logo">
                        
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="header__nav">
                            <nav class="header__menu">
                                <ul class="menu__class">
                                    <li><a href="#">Beranda</a></li>
                                    <li><a href="#">Berita</a></li>
                                    <li><a href="#">Organisasi</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Profil</a>
                                          
                                            </li>
                                            <li><a href="#">Perdata dan Tata Usaha Negara</a></li>
                                            <li><a href="#">Intelejen</a></li>
                                            <li><a href="#">Barang Bukti</a></li>
                                            <li><a href="#">Pembinaan</a></li>
                                            <li><a href="#">Pidana Khusus</a></li>
                                            <li><a href="#">Pidana Umum</a></li>
                                        </ul>
                                    </li>

                                     <li><a href="#">Zona Integritas</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Pengaduan Masyarakat</a></li>
                                            <li><a href="#">SIPIT - Tilang Online</a></li>
                                            <li><a href="#">Peta Informasi Digital</a></li>
                                            <li><a href="#">Datun Online</a></li>
                                            <li><a href="#">Whistle Blowing System</a></li>
                                            <li><a href="#">Indeks Kepuasan Pelayanan</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Informasi</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Galeri Foto</a></li>
                                            <li><a href="#">Galeri Video</a></li>
                                            <li><a href="#">Daftar Pencarian Orang</a></li>
                                            <li><a href="#">Agenda Kegiatan</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Download File</a></li>


                                </ul>
                            </nav>
                           
                        </div>
                    </div>
                </div>
                <div class="canvas__open">
                    <span class="fa fa-bars"></span>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Section End -->