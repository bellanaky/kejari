


    <!-- Hero Section Begin -->
    <section class="hero spad set-bg" data-setbg="<?php echo site_url($front_dir)?>/img/hero_2.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hero__text">
                        <h5>Kejaksaan Negeri Lampung Selatan</h5>
                        <h2>PORTAL RESMI</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Home About Section Begin -->
    <section class="home-about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="home__about__text">
                        <div class="section-title">
                            <h5>SELAYANG PANDANG</h5>
                            <h2>Selamat Datang di Portal Resmi Kejaksaan Negeri Lampung Selatan</h2>
                        </div>
                        <p class="first-para">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="home__about__pic">
                        <img src="<?php echo site_url($front_dir)?>/img/home-about/selayang-pandang.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Home About Section End -->

    <!-- Services Section Begin -->
    <section class="services spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <img src="<?php echo site_url($front_dir)?>/img/services/services-1.png" alt="">
                        <h4>E-Tilang</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <img src="<?php echo site_url($front_dir)?>/img/services/services-2.png" alt="">
                        <h4>Peta Informasi </h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <img src="<?php echo site_url($front_dir)?>/img/services/services-3.png" alt="">
                        <h4>Survey Pelayanan</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <img src="<?php echo site_url($front_dir)?>/img/services/services-4.png" alt="">
                        <h4>Pengaduan Masyarakat</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <img src="<?php echo site_url($front_dir)?>/img/services/services-6.png" alt="">

                        <h4>Datun Online</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <img src="<?php echo site_url($front_dir)?>/img/services/services-5.png" alt="">

                        <h4>Whistle Blowing System</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Services Section End -->


    <!-- Testimonial Section Begin -->
    <section class="testimonial spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="testimonial__pic">
                        <img src="<?php echo site_url($front_dir)?>/img/berita-terbaru.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="testimonial__text">
                        <div class="section-title">
                            <h2>Berita Terbaru</h2>
                            <h5>Topik Menarik Seputar Kejaksaan Negeri Lampung Selatan</h5>
                        </div>
                        <div class="testimonial__slider__content">
                            <div class="testimonial__slider owl-carousel">

                                <div class="testimonial__item">
                                    <h5>Judul Berita</h5>
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <div class="testimonial__author">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="testimonial__author__title">
                                                    <h5>Nama Penulis Berita</h5>
                                                    <span>Kontributor</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="testimonial__author__social">
                                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="testimonial__item">
                                    <h5>Judul Berita 2</h5>
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <div class="testimonial__author">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="testimonial__author__title">
                                                    <h5>Nama Penulis Berita</h5>
                                                    <span>Kontributor</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="testimonial__author__social">
                                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="testimonial__item">
                                    <h5>Judul Berita 3</h5>
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <div class="testimonial__author">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="testimonial__author__title">
                                                    <h5>Nama Penulis Berita</h5>
                                                    <span>Kontributor</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="testimonial__author__social">
                                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            
                                <div class="testimonial__item">
                                    <h5>Judul Berita 4</h5>
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <div class="testimonial__author">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="testimonial__author__title">
                                                    <h5>Nama Penulis Berita</h5>
                                                    <span>Kontributor</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="testimonial__author__social">
                                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="slide-num" id="snh-1"></div>
                            <div class="slider__progress"><span></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->


    <!-- Chooseus Section Begin -->
    <div class="chooseus spad set-bg" data-setbg="<?php echo site_url($front_dir)?>/img/back-berita.jpg">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8 text-center">
                    <div class="chooseus__text">
                        <div class="section-title">
                            <h5>BERITA UTAMA</h5>
                            <h2>Temukan Berita Penting dan Interaktif di Kejaksaan Negeri Lampung Selatan</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Chooseus Section End -->

  <!-- Blog Section Begin -->
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="blog__item__large">
                        <div class="blog__item__large__pic">
                            <img src="<?php echo site_url($front_dir)?>/img/blog/blog-2.jpg" alt="">
                            <div class="tag">Kategori 1</div>
                        </div>
                        <div class="blog__item__large__text">
                            <p><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y')?></p>
                            <h4><a href="#">Berita Menarik</a></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="blog__item">
                                <div class="blog__item__pic">
                                    <img src="<?php echo site_url($front_dir)?>/img/blog/blog-1.jpg" alt="">
                                    <div class="tag">Kategori 1</div>
                                </div>
                                <div class="blog__item__text">
                                    <p><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y')?></p>
                                    <h5><a href="#">Judul berita 1</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="blog__item">
                                <div class="blog__item__pic">
                                    <img src="<?php echo site_url($front_dir)?>/img/blog/blog-2.jpg" alt="">
                                    <div class="tag">Kategori 2</div>
                                </div>
                                <div class="blog__item__text">
                                    <p><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y')?></p>
                                    <h5><a href="#">Judul berita 2</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="blog__item">
                                <div class="blog__item__pic">
                                    <img src="<?php echo site_url($front_dir)?>/img/blog/blog-3.jpg" alt="">
                                    <div class="tag">Kategori 3</div>
                                </div>
                                <div class="blog__item__text">
                                    <p><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y')?></p>
                                    <h5><a href="#">Judul berita 3</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="blog__item">
                                <div class="blog__item__pic">
                                    <img src="<?php echo site_url($front_dir)?>/img/blog/blog-4.jpg" alt="">
                                    <div class="tag">Kategori 4</div>
                                </div>
                                <div class="blog__item__text">
                                    <p><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y')?></p>
                                    <h5><a href="#">Judul berita 4</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="blog__item">
                                <div class="blog__item__pic">
                                    <img src="<?php echo site_url($front_dir)?>/img/blog/blog-5.jpg" alt="">
                                    <div class="tag">Kategori 5</div>
                                </div>
                                <div class="blog__item__text">
                                    <p><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y')?></p>
                                    <h5><a href="#">Judul berita 5</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="blog__item">
                                <div class="blog__item__pic">
                                    <img src="<?php echo site_url($front_dir)?>/img/blog/blog-4.jpg" alt="">
                                    <div class="tag">Kategori 6</div>

                                </div>
                                <div class="blog__item__text">
                                    <p><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y')?></p>
                                    <h5><a href="#">Judul berita 6</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="pagination__number blog__pagination">
                                <a href="#">1</a>
                                <a href="#">2</a>
                                <a href="#">Selanjutnya <span class="arrow_right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="blog__sidebar">
                        <div class="blog__sidebar__search">
                            <h4>Cari Berita</h4>
                            <form action="#">
                                <input type="text" placeholder="Ketik Pencarian...">
                                <button type="submit">Cari</button>
                            </form>
                        </div>

                         <div class="blog__sidebar__recent">
                            <h4>Video</h4>
                            <a href="#" class="blog__sidebar__recent__item">
                                <div class="blog__sidebar__recent__item__pic">
                                    <img src="<?php echo site_url($front_dir)?>/img/blog/sidebar/recent-1.jpg" alt="">
                                </div>
                                <div class="blog__sidebar__recent__item__text">
                                    <h6>Smart HVAC Solutions Keep It</h6>
                                    <div class="time"><i class="fa fa-clock-o"></i> 01th March, 2019</div>
                                </div>
                            </a>
                           
                        </div>

                        <div class="blog__sidebar__recent">
                            <h4>Postingan Terbaru</h4>
                            <a href="#" class="blog__sidebar__recent__item">
                                <div class="blog__sidebar__recent__item__pic">
                                    <img src="<?php echo site_url($front_dir)?>/img/blog/sidebar/recent-1.jpg" alt="">
                                </div>
                                <div class="blog__sidebar__recent__item__text">
                                    <h6>Smart HVAC Solutions Keep It</h6>
                                    <div class="time"><i class="fa fa-clock-o"></i> 01th March, 2019</div>
                                </div>
                            </a>
                            <a href="#" class="blog__sidebar__recent__item">
                                <div class="blog__sidebar__recent__item__pic">
                                    <img src="<?php echo site_url($front_dir)?>/img/blog/sidebar/recent-2.jpg" alt="">
                                </div>
                                <div class="blog__sidebar__recent__item__text">
                                    <h6>Guests think these services</h6>
                                    <div class="time"><i class="fa fa-clock-o"></i> 02th March, 2019</div>
                                </div>
                            </a>
                            <a href="#" class="blog__sidebar__recent__item">
                                <div class="blog__sidebar__recent__item__pic">
                                    <img src="<?php echo site_url($front_dir)?>/img/blog/sidebar/recent-3.jpg" alt="">
                                </div>
                                <div class="blog__sidebar__recent__item__text">
                                    <h6>Europe's 2018 hotel strengths</h6>
                                    <div class="time"><i class="fa fa-clock-o"></i> 03th March, 2019</div>
                                </div>
                            </a>
                        </div>
                        <div class="blog__sidebar__categories">
                            <h4>Kategori Berita</h4>
                            <ul>
                                <li><a href="#">Life Style</a></li>
                                <li><a href="#">Photography</a></li>
                                <li><a href="#">Work</a></li>
                                <li><a href="#">Travel</a></li>
                                <li><a href="#">Sport</a></li>
                            </ul>
                        </div>
                        <div class="blog__sidebar__comment">
                            <h4>Komentar berita-terbaru</h4>
                            <p>Attending a trade show can be a very effective method of promoting your company and its
                            </p>
                            <span>Smart HVAC Solutions Keep It Comfortable</span>
                            <p>When I was just starting 6th grade I got my first job. Paperboy! Boy, was I excited. At
                                that</p>
                            <span>Guests think these services would be easier</span>
                            <p>Judul Berita Satu
                            </p>
                            <span>Europe's 2018 hotel strengths belie</span>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section End -->


    <!-- Latest Blog Section Begin -->
    <section class="latest-blog ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>KEGIATAN KEJAKSAAN</h2>
                        <h5>Mari Kita Lihat Agenda Terdekat Kejaksaan Negeri Lampung Selatan</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 p-0 order-lg-1 col-md-6 order-md-1">
                    <div class="latest__blog__pic set-bg" data-setbg="<?php echo site_url($front_dir);?>/img/latest-blog/lb-1.jpg"></div>
                </div>
                <div class="col-lg-3 p-0 order-lg-2 col-md-6 order-md-2">
                    <div class="latest__blog__text">
                        <div class="label">Kegiatan 1</div>
                        <h5>Nama Kegiatan 1</h5>
                        <p><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y')?></p>
                        <a href="#">Selengkapnya</a>
                    </div>
                </div>
                <div class="col-lg-3 p-0 order-lg-3 col-md-6 order-md-4">
                    <div class="latest__blog__pic set-bg" data-setbg="<?php echo site_url($front_dir);?>/img/latest-blog/lb-2.jpg"></div>
                </div>
                <div class="col-lg-3 p-0 order-lg-4 col-md-6 order-md-3">
                    <div class="latest__blog__text">
                        <div class="label">Kegiatan 2</div>
                        <h5>Nama Kegiatan 2</h5>
                        <p><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y')?></p>
                        <a href="#">Selengkapnya</a>
                    </div>
                </div>
                <div class="col-lg-3 p-0 order-lg-6 col-md-6 order-md-5">
                    <div class="latest__blog__pic latest__blog__pic__last__row set-bg"
                        data-setbg="<?php echo site_url($front_dir);?>/img/latest-blog/lb-3.jpg"></div>
                </div>
                <div class="col-lg-3 p-0 order-lg-5 col-md-6 order-md-6">
                    <div class="latest__blog__text">
                        <div class="label">Kegiatan 3</div>
                        <h5>Nama Kegiatan 3</h5>
                        <p><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y')?></p>
                        <a href="#">Selengkapnya</a>
                    </div>
                </div>
                <div class="col-lg-3 p-0 order-lg-8 col-md-6 order-md-8">
                    <div class="latest__blog__pic latest__blog__pic__last__row set-bg"
                        data-setbg="<?php echo site_url($front_dir);?>/img/latest-blog/lb-4.jpg"></div>
                </div>
                <div class="col-lg-3 p-0 order-lg-7 col-md-6 order-md-7">
                    <div class="latest__blog__text">
                         <div class="label">Kegiatan 4</div>
                        <h5>Nama Kegiatan 4</h5>
                        <p><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y')?></p>
                        <a href="#">Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Latest Blog Section End -->

 
</body>

</html>